package com.webside.base.basedao;

/**
 * @author lizx
 * @data 2016-9-9上午11:26:48
 */
import java.util.Collection;
import java.util.List;
public interface BaseDao {
	
	/**
	 * 查询列表数据
	 */
	public List selectList(String sqlName, Object obj);
	/**
	 * 查询列表数据
	 */
	public List selectList(String sqlName);
	/**
	 * 查询一条数据
	 * @param sqlName
	 * @param obj
	 * @return
	 */
	public Object selectOne(String sqlName, Object obj);
	
	/**
	 * 更新数据
	 * @param sqlName
	 * @param obj
	 * @return
	 */
	public int update(String sqlName, Object obj);
	
	/**
	 * 插入一条数据
	 * @param sqlName
	 * @param obj
	 * @return
	 */
	public int insert(String sqlName, Object obj);
	
	/**
	 * 删除数据
	 * @param sqlName
	 * @param obj
	 * @return
	 */
	public int delete(String sqlName, Object obj);
	
	public void updateBatch(String sqlName, Collection list);
	
	/**
	 * 查询分页数据
	 * @param sql
	 * @param parameterObject
	 * @param skipResults
	 * @param maxResults
	 * @return
	 */
	public List getPage(String sql, Object parameterObject, int skipResults, int maxResults);
	
	/**
	 * 运行期获取MyBatis执行的SQL及参数
	 * 
	 * @param id
	 *            Mapper xml 文件里的sql的 Id
	 * @param parameterObject
	 *            参数
	 * @return
	 */
	public MyBatisSql getMyBatisSql(String id, Object parameterObject);
	
	public void setAutoCommit(boolean autoCommit);
}
