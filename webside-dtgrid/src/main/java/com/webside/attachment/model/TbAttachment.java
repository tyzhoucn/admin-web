package com.webside.attachment.model;

import org.apache.ibatis.type.Alias;

import com.webside.base.basemodel.BaseEntity;

@Alias("attachmentEntity")
public class TbAttachment extends BaseEntity {

  private String fileName;
  private String filePath;
  private Long type;
  private Long typeId;

  public Long getType() {
    return type;
  }

  public Long getTypeId() {
    return typeId;
  }

  public void setTypeId(Long typeId) {
    this.typeId = typeId;
  }

  public String getFilePath() {
    return filePath;
  }

  public void setFilePath(String filePath) {
    this.filePath = filePath;
  }

  public void setType(Long type) {
    this.type = type;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  @Override
  public String toString() {
    return "TbAttachment{" +
            "id=" + id +
            ", fileName='" + fileName + '\'' +
            ", filePath='" + filePath + '\'' +
            ", type=" + type +
            ", typeId=" + typeId +
            '}';
  }
}
